db.fruits.insertMany([
{
name : "Apple",
color : "Red",
stock : 20,
price: 40,
supplier_id : 1,
onSale : true,
origin: [ "Philippines", "US" ]
},
{
name : "Banana",
color : "Yellow",
stock : 15,
price: 20,
supplier_id : 2,
onSale : true,
origin: [ "Philippines", "Ecuador" ]
},
{
name : "Kiwi",
color : "Green",
stock : 25,
price: 50,
supplier_id : 1,
onSale : true,
origin: [ "US", "China" ]
},
{
name : "Mango",
color : "Yellow",
stock : 10,
price: 120,
supplier_id : 2,
onSale : false,
origin: [ "Philippines", "India" ]
}
]);

/*
	AGGREGATION
	- definition

	AGREGATE METHODS
	1. Match
	2. Group
	3. SOrt
	4. Project

	AGGREGATION PIPELINES

	OPERATORS
	1. Sum
	2. Max
	3. Min
	4. Avg
*/
//MONGODB AGREGATION

/*
		-Used to generate manipulated data and perform operations to create a filtered results that helps analyzing data.
		- Compared to doing CRUD Operations on our data from our previous sessioms, aggregation give us access to manipulate, filter and compute for results providing us with information to take necessay development decisions
		- Aggregations in MONGODB are ver flexible and you can form you on aggregation pipeline depending on the need of your application

*/
// AGGREGATE METHoDS

/*
		#1 MATCH : "$match"
	- The "$match" is used to pass the documents that meet the specified condition(s) to the next pipeline stage/ aggreagation process

	Syntax:
	{ $match: {field: value}}

*/

db.fruits.aggregate([
{$match: {onSale: true}}
]);

db.fruits.aggregate([
		{ $match: { stock: { $gte: 20 } } }
]);


/*
		#2 GROUP: "$group"
		- The "$group" is used to group elements together field -value paris using teh data from the grouped element.
		- Syntax
			{ $group: {-id: "value", fieldResult: "valueResult"} }
*/

db.fruits.aggregate([
{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);

/*
	#3 SORT: "$sort"
	- the "$sort" can be used when changing the order of aggregated results
	- Providing a value of -1 will sort the documents in a descending order
	- Providing a value of 1 will sort the documents in an ascending order

	-Syntax:
	{ $sort { field: 1/-1} }
*/

db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
	{ $sort: {total: -1} }
]);

/*
	#4 PROJECT: "$project"

	- The "$project" can be used when aggregating data to include or exclude fields from the returned results

	Syntax:

		{$project: {field: 1/0}}
*/

db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $group: {_id: "$supplier_id", total: {$sum: {"$stock"}}},
		{ $project: {_id: 0}}
]);

db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{ $project: {_id: 0}},
		{ $sort: {total: -1}}
		
]);		

/*
	AGGREGATION PIPELINES
	- THe aggregation pipeline in MongoDB is a framework fro data aggregation

	- Each stage transforms the documents as they pass through the deadlines

	- Syntax:
		db.collectionName.aggregate([
			{stageA},
			{stageB},
			{stageC}

		])
*/

// OPERATORS
// #1 "$sum" - gets the total of everything

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"} } }

]);

// #2 "$max" - gets the highest value out of everything else

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
]);

// #3 "$min" - gets the lowest value out of everything else 
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
]);

// #4 "$avg" - get the average value of all the fields
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);